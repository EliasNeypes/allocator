// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s
#include <sstream>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include "Allocator.h"

using namespace std;
// -----------
// getNumCases
// -----------
#define SIZE 1000
// @return: number of cases following.
int getNumCases(istream& r) {
    string t;
    getline(r, t);
    istringstream sin(t);
    int numCases;
    sin >> numCases;
    getline(r, t);
    return numCases;
}

/* Execute one test case
 * @param r: read in stream
 * @param w: write out stream
 */
void alloc_case(istream& r, ostream& w) {
    // New allocator object for each case.
    my_allocator<double, SIZE> ma;
    vector<double*> ptrs;
    string s;
    // Retrieve and execute request
    while(getline(r, s) && !s.empty()) {
        istringstream sin(s);
        int request;
        sin >> request;
        if (request > 0) {
            double *addr = ma.allocate(request);
            if (addr)
                ptrs.push_back(addr);
        } else if (request < 0) {
            request = (request * -1) - 1;
            double *dealloc = ptrs[request];
            ma.deallocate(dealloc, 0);
            ptrs.erase(ptrs.begin() + request);
        }
        sort(begin(ptrs), end(ptrs));
    }
    // Iterate through array and print each block's sentinel val
    int index = 0;
    while (index < (int) SIZE) {
        int sentinel = ma[index];
        w << sentinel;
        int abs_sentinel = sentinel;
        if (sentinel < 0) {
            abs_sentinel *= -1;
        }
        index += abs_sentinel + (2 * sizeof(int));
        if (index < (int) (SIZE - sizeof(int))) w << " ";
    }
}

// ----
// main
// ----

int main () {


    freopen("RunAllocator.in", "r", stdin) ; // redirects standard input
    freopen("RunAllocator.out", "w", stdout); // redirects standard output

    // Reusing layout used in Elias N.'s & Ailyn A's CS371P Voting project:
    // https://gitlab.com/ailynailyn/voting
    int numCases = getNumCases(cin);
    assert(numCases > 0 && numCases < 100); // 0 < T < 100
    // Now in a loop of numCases times, do the eval.
    while(numCases > 0) {
        alloc_case(cin, cout);
        numCases--;
        if (numCases != 0) {
            cout << endl;
        }
    }

    return 0;
}
