// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------
#include <iostream>
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include "gtest/gtest.h"

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    FRIEND_TEST(ValidTest, valid1);
    FRIEND_TEST(ValidTest, valid2);
    FRIEND_TEST(ValidTest, valid3);
    FRIEND_TEST(CoalesceTest, coalesce1);
    FRIEND_TEST(CoalesceTest, coalesce2);
    FRIEND_TEST(DivideTest, free_block_divide);
    FRIEND_TEST(DeallocateTest, deallocate1);
    FRIEND_TEST(DeallocateTest, deallocate2);
  public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

  public:
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

  private:
    // ----
    // data
    // ----

    char a[N];
    size_t _min_div;

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Iterate through a[] and guarantee each sentinel has a match and fits
     * bounds of the array.
     */
    bool valid () const {
        int index = 0;
        // Each loop checks one block of bytes and their sentinels.
        while ((u_int) index < N) {
            int sentinel = *((int *) (a + index));
            // Make sure we don't jump backwards.
            int abs_sentinel = sentinel;
            if (sentinel < 0) {
                abs_sentinel *= -1;
            }
            index += abs_sentinel + sizeof(int);
            if (*((int *)(a+index)) != sentinel) {
                return false;
            }
            index += sizeof(int);

        }
        return true;
    }


    // -----
    // free_block_divide
    // -----

    /**
     * O(1) in space
     * O(1) in time
     * @param index, index of the start of the found free block
     * @param bytes, size to create subdivision of
     */
    void free_block_divide(int index, size_t bytes) {
        assert((u_int) (index + bytes) < N);
        int orig_size = this->operator[](index);
        int orig_end_ind = index + this->operator[](index) + sizeof(int);
        int new_sent_ind_1 = index + bytes + sizeof(int);
        int new_sent_ind_2 = new_sent_ind_1 + sizeof(int);

        int *orig_addr =       (int *)(a + index);
        int *new_sent_addr_1 = (int *)(a + new_sent_ind_1);
        int *new_sent_addr_2 = (int *)(a + new_sent_ind_2);
        int *orig_end_addr =   (int *)(a + orig_end_ind);


        *orig_addr =    (int) bytes * -1;
        *new_sent_addr_1 = (int) bytes * -1;
        *new_sent_addr_2 = (int) (orig_size - (bytes + (sizeof(int) * 2)));
        *orig_end_addr = (int) *new_sent_addr_2;
    }

    // --------
    // coalesce
    // --------

    /**
     * O(1) in space
     * O(1) in time
     * @param first, index of a starting sentinel for first block
     * @param second, index of a starting sentinel for second blocks
     * @return first, the index of the coalesced block's starting sentinel.
     */
    int coalesce(int first, int second) {
        assert(first < second);
        int first_val = this->operator[](first);
        int sec_val = this->operator[](second);
        assert(first_val > 0 && sec_val > 0);
        int new_sent_val = first_val + sec_val + (2 * sizeof(int));
        this->operator[](first) = new_sent_val;
        this->operator[](first + new_sent_val + sizeof(int)) = new_sent_val;
        return first;
    }


  public:
    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () :
        _min_div(sizeof(T) + (2 * sizeof(int))) {
        if (N < (u_int) (sizeof(T) + (2 * sizeof(int)))) {
            throw std::bad_alloc();
        }

        int r = N - (sizeof(int) * 2);
        int *sent = (int *)a;
        *sent = r;
        int *end_sent = (int*)(a + (N - sizeof(int)));
        *end_sent = r;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int)), else
     * give the whole block.
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type s) {
        int bytes = (s * sizeof(T));
        int min_need = bytes + (sizeof(int) * 2) + sizeof(T);
        int index = 0;
        bool found = false;
        bool no_subdiv = false;
        int sentinel;
        // Find a good/perfect fit free-block
        while (!found && (u_int) index < N) {
            sentinel = this->operator[](index);
            if (sentinel == bytes) { // Exact fit
                found = true;
                no_subdiv = true;
            } else if (sentinel >= min_need) { // enough for Sub_div case
                found = true;
            } else { // No fit
                int abs_sentinel = sentinel;
                if (sentinel < 0) {
                    abs_sentinel *= -1;
                }
                index = index + abs_sentinel +  (2 * sizeof(int));
                assert(sentinel == this->operator[](index - sizeof(int)));
            }
        }
        // Try and find fit with extra space.
        if (!found) index = 0;
        while(!found && (u_int) index < N) {
            sentinel = this->operator[](index);
            if (sentinel < bytes) { // No fit
                int abs_sentinel = sentinel;
                if (sentinel < 0) {
                    abs_sentinel *= -1;
                }
                index = index + abs_sentinel +  (2 * sizeof(int));
                assert(sentinel == this->operator[](index - sizeof(int)));

            } else if (bytes <= sentinel && sentinel < min_need) {
                found = true;
                no_subdiv = true;
            }
        }
        // No blocks could hold the request
        if (!found) {
            throw std::bad_alloc();
        }
        if (no_subdiv) {
            this->operator[](index) *= -1;
            this->operator[](index + sentinel + sizeof(int)) *= -1;
        } else {
            free_block_divide(index, bytes);
        }
        index += sizeof(int);
        assert(valid());
        return (pointer) (a + index);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Coalesces adjacent freeblocks after initial freeing.
     */
    void deallocate (pointer _p, size_type s) {
        // Get rid of unused warning
        (void)s;
        char * p = (char *)_p;
        // Check p is a part of the allocator's data,
        if (p < a || this->operator[]((p - a) - sizeof(int)) > 0) {
            throw std::invalid_argument("Invalid pointer");
        }
        int index = (p - a) - sizeof(int);
        int range = this->operator[](index);
        this->operator[](index) = range * -1;
        int end_ind = index + (range * -1) + sizeof(int);
        this->operator[](end_ind) = range * -1;
        int prev_end = index - sizeof(int);
        int next_ind = end_ind + sizeof(int);
        if (prev_end > 0 && this->operator[](prev_end) > 0) {
            index = coalesce(prev_end - this->operator[](prev_end) - sizeof(int), index);
        }
        if ((u_int) next_ind < N  && this->operator[](next_ind) > 0) {
            coalesce(index, next_ind);
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }


    /**
     * Print the sentinel val associated with p.
     * ASSUMES p is the first element in a block.
     */
    int block_sentinel(const pointer _p) {
        char *p = (char *) _p;
        int index = (p - a) - sizeof(int);
        return this->operator[](index);
    }
};


#endif // Allocator_h
