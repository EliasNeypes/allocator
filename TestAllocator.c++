// -----------------------------------------
// projects/c++/allocator/TestAllocator1.c++
// Copyright (C) 2017
// Glenn P. Downing
// -----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"
#include<vector>



// -----
// valid
// -----



TEST(ValidTest, valid1) {
    my_allocator<int, 100> x;
    EXPECT_TRUE(x.valid());
}

TEST(ValidTest, valid2) {
    my_allocator<int, 100> x;
    x.allocate(5);
    EXPECT_TRUE(x.valid());
}


TEST(ValidTest, valid3) {
    my_allocator<int, 100> x;
    x.allocate(5);
    x.a[0] = 92;
    EXPECT_FALSE(x.valid());
}


TEST(ConstructTest, construct_dbl) {
    my_allocator<double, 1000> x;
    x.allocate(5);
    ASSERT_EQ(x[0], -40);
}

TEST(DivideTest, free_block_divide) {
    my_allocator<double, 1000> x;
    x[0] = -40;
    x[44] = -40;
    x[48] = 944;
    x[996] = 944;
    x.free_block_divide(48, 800);
    ASSERT_EQ(x[0], -40);
    ASSERT_EQ(x[48], -800);
    ASSERT_EQ(x[856], 136);
    EXPECT_TRUE(x.valid());
}

TEST(CoalesceTest, coalesce1) {
    my_allocator<double, 1000> x;
    std::vector<double*> ptrs;
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(5));
    x.deallocate(ptrs[1], 0);
    x.deallocate(ptrs[2], 0);
}

TEST(CoalesceTest, coalesce2) {
    my_allocator<double, 1000> x;
    std::vector<double*> ptrs;
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(5));
    EXPECT_EQ(x[0], -40);
    EXPECT_EQ(x[48], -40);
    EXPECT_EQ(x[96], -40);
    EXPECT_EQ(x[144], -40);
    x.deallocate(ptrs[0], 0);
    x.deallocate(ptrs[2], 0);
    x.deallocate(ptrs[1], 0);
    EXPECT_EQ(x[0], 136);
}


TEST(DeallocateTest, deallocate1) {
    my_allocator<double, 1000> x;
    std::vector<double*> ptrs;
    ptrs.push_back(x.allocate(5));
    ptrs.push_back(x.allocate(3));

    ptrs.push_back(x.allocate(3));
    int index = 0;
    while (index < 1000) {
        int sentinel = x[index];
        int abs_sentinel = sentinel;
        if (sentinel < 0) {
            abs_sentinel *= -1;
        }
        index += abs_sentinel + (2 * sizeof(int));
    }

    x.deallocate(ptrs[0], 0);
    x.deallocate(ptrs[1], 0);
    index = 0;
    while (index < 1000) {

        int sentinel = x[index];
        int abs_sentinel = sentinel;
        if (sentinel < 0) {
            abs_sentinel *= -1;
        }
        index += abs_sentinel + (2 * sizeof(int));
        ASSERT_EQ(sentinel, x[index - sizeof(int)]);
    }
    ASSERT_EQ(index, 1000);

}

TEST(DeallocateTest, deallocate2) {
    my_allocator<double, 1000> x;
    std::vector<double*> ptrs;
    ptrs.push_back(x.allocate(124));
    x.deallocate(ptrs[0], 0);
    ASSERT_EQ(x[0], 992);
}

TEST(MixedTest, mixed1) {
    my_allocator<double, 1000> x;
    std::vector<double*> ptrs;
    ptrs.push_back(x.allocate(124));
    ASSERT_EQ(x[0], -992);

    sort(begin(ptrs), end(ptrs));
    x.deallocate(ptrs[0], 0);
    ptrs.erase(ptrs.begin() + 0);
    ASSERT_EQ(x[0], 992);

    ptrs.push_back(x.allocate(61));
    ASSERT_EQ(x[0], -488);
    ASSERT_EQ(x[496], 496);
    ptrs.push_back(x.allocate(61));

    ASSERT_EQ(x[0], -488);
    ASSERT_EQ(x[496], -496);
    x.deallocate(ptrs[1], 0);
    ptrs.erase(ptrs.begin() + 1);
    x.deallocate(ptrs[0], 0);
    ptrs.erase(ptrs.begin() + 0);
    ASSERT_EQ(x[0], 992);

    ptrs.push_back(x.allocate(30));
    ptrs.push_back(x.allocate(30));
    ptrs.push_back(x.allocate(30));
    ptrs.push_back(x.allocate(30));

    x.deallocate(ptrs[3], 0);
    ptrs.erase(ptrs.begin() + 3);
    x.deallocate(ptrs[0], 0);
    ptrs.erase(ptrs.begin() + 0);
    x.deallocate(ptrs[1], 0);
    ptrs.erase(ptrs.begin() + 1);
    x.deallocate(ptrs[0], 0);
    ptrs.erase(ptrs.begin() + 0);

    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ptrs.push_back(x.allocate(10));
    ASSERT_EQ(x[704], 288);
}




// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type  x;
    const size_type       s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        } catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

TEST(TestAllocator2, const_index) {
    const my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

TEST(TestAllocator2, index) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 10;
    const value_type     v = 2;
    const pointer        b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        } catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}
